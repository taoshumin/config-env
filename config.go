/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package env

// Configuration configures and creates Jaeger Tracer
type Configuration struct {
	// ServiceName specifies the service name to use on the tracer.
	// Can be provided by FromEnv() via the environment variable named JAEGER_SERVICE_NAME
	ServiceName string `yaml:"serviceName"`

	// Disabled makes the config return opentracing.NoopTracer.
	// Value can be provided by FromEnv() via the environment variable named JAEGER_DISABLED.
	Disabled bool `yaml:"disabled"`

	// RPCMetrics enables generations of RPC metrics (requires metrics factory to be provided).
	// Value can be provided by FromEnv() via the environment variable named JAEGER_RPC_METRICS
	RPCMetrics bool `yaml:"rpc_metrics"`

	// Gen128Bit instructs the tracer to generate 128-bit wide trace IDs, compatible with W3C Trace Context.
	// Value can be provided by FromEnv() via the environment variable named JAEGER_TRACEID_128BIT.
	Gen128Bit bool `yaml:"traceid_128bit"`

	// Tags can be provided by FromEnv() via the environment variable named JAEGER_TAGS
	Tags []Tag `yaml:"tags"`

	Sampler *SamplerConfig `yaml:"sampler"`
}

// SamplerConfig allows initializing a non-default sampler.  All fields are optional.
type SamplerConfig struct {
	// Type specifies the type of the sampler: const, probabilistic, rateLimiting, or remote.
	// Can be provided by FromEnv() via the environment variable named JAEGER_SAMPLER_TYPE
	Type string `yaml:"type"`

	// Param is a value passed to the sampler.
	// Valid values for Param field are:
	// - for "const" sampler, 0 or 1 for always false/true respectively
	// - for "probabilistic" sampler, a probability between 0 and 1
	// - for "rateLimiting" sampler, the number of spans per second
	// - for "remote" sampler, param is the same as for "probabilistic"
	//   and indicates the initial sampling rate before the actual one
	//   is received from the mothership.
	// Can be provided by FromEnv() via the environment variable named JAEGER_SAMPLER_PARAM
	Param float64 `yaml:"param"`
}

// Tag Tag{"key", value}.Set(span)
type Tag struct {
	Key   string
	Value interface{}
}
