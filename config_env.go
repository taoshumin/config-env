/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package env

import (
	"github.com/pkg/errors"
	"os"
	"strconv"
	"strings"
)

const (
	envServiceName  = "JAEGER_SERVICE_NAME"
	envDisabled     = "JAEGER_DISABLED"
	envRPCMetrics   = "JAEGER_RPC_METRICS"
	env128bit       = "JAEGER_TRACEID_128BIT"
	envTags         = "JAEGER_TAGS"
	envSamplerType  = "JAEGER_SAMPLER_TYPE"
	envSamplerParam = "JAEGER_SAMPLER_PARAM"
)

// FromEnv uses environment variables to set the tracer's Configuration
func FromEnv() (*Configuration, error) {
	c := &Configuration{}
	return c.FromEnv()
}

// FromEnv uses environment variables and overrides existing tracer's Configuration
func (c *Configuration) FromEnv() (*Configuration, error) {
	if e := os.Getenv(envServiceName); e != "" {
		c.ServiceName = e
	}

	if e := os.Getenv(envDisabled); e != "" {
		if value, err := strconv.ParseBool(e); err == nil {
			c.Disabled = value
		} else {
			return nil, errors.Wrapf(err, "cannot parse env var %s=%s", envDisabled, e)
		}
	}

	if e := os.Getenv(envRPCMetrics); e != "" {
		if value, err := strconv.ParseBool(e); err == nil {
			c.RPCMetrics = value
		} else {
			return nil, errors.Wrapf(err, "cannot parse env var %s=%s", envRPCMetrics, e)
		}
	}

	if e := os.Getenv(env128bit); e != "" {
		if value, err := strconv.ParseBool(e); err == nil {
			c.Gen128Bit = value
		} else {
			return nil, errors.Wrapf(err, "cannot parse env var %s=%s", env128bit, e)
		}
	}

	if e := os.Getenv(envTags); e != "" {
		c.Tags = parseTags(e)
	}

	if s, err := c.Sampler.samplerConfigFromEnv(); err == nil {
		c.Sampler = s
	} else {
		return nil, errors.Wrap(err, "cannot obtain sampler config from env")
	}

	return c, nil
}

// samplerConfigFromEnv creates a new SamplerConfig based on the environment variables
func (sc *SamplerConfig) samplerConfigFromEnv() (*SamplerConfig, error) {
	if e := os.Getenv(envSamplerType); e != "" {
		sc.Type = e
	}

	if e := os.Getenv(envSamplerParam); e != "" {
		if value, err := strconv.ParseFloat(e, 64); err == nil {
			sc.Param = value
		} else {
			return nil, errors.Wrapf(err, "cannot parse env var %s=%s", envSamplerParam, e)
		}
	}

	return sc, nil
}

// parseTags parses the given string into a collection of Tags.
// Spec for this value:
// - comma separated list of key=value
// - value can be specified using the notation ${envVar:defaultValue}, where `envVar`
// is an environment variable and `defaultValue` is the value to use in case the env var is not set
func parseTags(sTags string) []Tag {
	pairs := strings.Split(sTags, ",")
	tags := make([]Tag, 0)
	for _, p := range pairs {
		kv := strings.SplitN(p, "=", 2)
		k, v := strings.TrimSpace(kv[0]), strings.TrimSpace(kv[1])

		if strings.HasPrefix(v, "${") && strings.HasSuffix(v, "}") {
			ed := strings.SplitN(v[2:len(v)-1], ":", 2)
			e, d := ed[0], ed[1]
			v = os.Getenv(e)
			if v == "" && d != "" {
				v = d
			}
		}

		tag := Tag{Key: k, Value: v}
		tags = append(tags, tag)
	}

	return tags
}

